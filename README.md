
# Leroy-Merlin-Air-Project-Java

Naive implementation of drone deliveries, in 2D Euclidean space


## Build With
Java v17.0.2

Dependency : 
 * com.opencsv v5.6
 * org.junit.jupiter 
 * org.mockito.core v4

 


## Run Locally


```bash
  $ mvn compile exec:java -Dexec.mainClass="Run"
```

See result at ..\src\main\resources\csv\flight-plans.csv