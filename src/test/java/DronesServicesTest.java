import models.Drone;
import models.dto.DroneDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import repositories.DronesRepository;
import services.DronesServices;
import services.Logger;

import java.util.Arrays;
import java.util.List;

public class DronesServicesTest {

    private final String DRONE_ID = "A";
    @InjectMocks
    private DronesServices dronesServices;
    private DronesRepository dronesRepository;
    private Logger logger;

    private static Drone createDrone(String id) {
        Drone drone = new Drone();
        drone.setId(id);
        drone.setX((int) (Math.random() * 100));
        drone.setY((int) (Math.random() * 100));
        return drone;
    }

    @BeforeEach
    public void init() {
        dronesRepository = Mockito.mock(DronesRepository.class);
        logger = Mockito.mock(Logger.class);
        Mockito.when(dronesRepository.retrieveDrones()).thenReturn(Arrays.asList(createDrone(DRONE_ID), createDrone("B"), createDrone("C")));
        Mockito.doNothing().when(logger).info(Mockito.anyString());
        dronesServices = DronesServices.getInstance(logger, dronesRepository);
    }

    @Test
    public void shouldRetrieveAvailableDrones() {
        List<DroneDTO> drones = dronesServices.getAvailableDrones();
        Assertions.assertEquals(drones.size(), 3);
    }

    @Test
    public void shouldUpdateDroneInformation() {
        dronesServices.updateDronePosition(DRONE_ID, 0, 0, 50);
        List<DroneDTO> drones = dronesServices.getAvailableDrones();

        Assertions.assertEquals(drones.size(), 3);
        DroneDTO d = drones.stream().filter(droneDTO -> droneDTO.getId().equals(DRONE_ID)).findFirst().get();

        Assertions.assertEquals(d.getX(), 0);
        Assertions.assertEquals(d.getY(), 0);

    }
}
