import models.Customer;
import models.Order;
import models.Store;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import repositories.CustomersRepository;
import repositories.OrdersRepository;
import repositories.common.CsvRepository;
import services.Logger;

import java.nio.file.Paths;
import java.util.List;

public class RepositoryTest {

    private static List<Order> orders;
    private static List<Customer> customers;
    private static Logger log;
    private CsvRepository csvRepository;

    @BeforeAll
    public static void beforeAll() {
        orders = OrdersRepository.getInstance().retrieveOrders();
        customers = CustomersRepository.getInstance().retrieveCustomers();
        log = Mockito.mock(Logger.class);
    }

    @Test
    public void sanityCheck() {
        Assertions.assertTrue(true);
    }

    @Test
    public void shouldCreateCsvReposity() {
        csvRepository = new CsvRepository(Paths.get("").toAbsolutePath() + "\\src\\test\\ressources\\csv\\stores.csv", Store.class, log);
        Assertions.assertNotNull(csvRepository);
    }

    @Test
    public void shouldHandleNotFoundException() {
        csvRepository = new CsvRepository("invalid path", Object.class, log);
        Mockito.verify(log, Mockito.times(1)).error(Mockito.anyString(), Mockito.any());
    }

//    @Test
//    public void shouldWriteBeans(){
//        String filePath = Paths.get("").toAbsolutePath() +  "\\src\\test\\ressources\\csv\\stores.csv";
//        csvRepository = new CsvRepository<Customer>(filePath, Store.class, log);
//        csvRepository.writeResult(customers, Paths.get("").toAbsolutePath() +  "\\src\\test\\ressources\\csv\\test-write.csv");
//
//        File file = new File(filePath);
//        Assertions.assertTrue(file.exists());
//    }

    @Test
    public void shouldRetrievesOrders() {
        Assertions.assertEquals(orders.size(), 3);
    }


    @Test
    public void shouldRetrieveOrdersDetails() {
        Order order = orders.get(0);
        List<String> product = order.getProducts().values().stream().toList();
        List<Integer> quantity = order.getQuantity().values().stream().toList();


        Assertions.assertEquals(order.getId(), "LMFRORDER-1");
        Assertions.assertEquals(order.getCustomerId(), "CUS-1");
        Assertions.assertEquals(product.size(), 3);
        Assertions.assertEquals(quantity.size(), 3);
        Assertions.assertEquals(product.get(0), "LMFRPRD-1");
        Assertions.assertEquals(quantity.get(0), Integer.valueOf(5));
    }


    @Test
    public void shouldRetrieveCustomers() {
        Assertions.assertEquals(customers.size(), 3);
    }

    @Test
    public void shouldRetrieveCustomersDetails() {
        Customer customer = customers.get(0);

        Assertions.assertEquals(customer.getId(), "CUS-1");
        Assertions.assertEquals(customer.getX(), 5);
        Assertions.assertEquals(customer.getY(), 8);
    }


}
