import models.common.Point;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import services.DistanceManagementService;

import java.util.Arrays;
import java.util.List;

public class DistanceCalculationTest {

    @Test
    public void shouldReturnTheRightDistanceBetween2Points() {
        Point a = createPoint(95, 65);
        Point b = createPoint(44, 65);
        Assertions.assertEquals(DistanceManagementService.getInstance().distance(a, b), 51);
    }

    @Test
    public void shouldReturnTheRightDistanceBetweenALineAndAPoint() {
        Point a = createPoint(5, 2);
        Point b = createPoint(6, 2);
        Point c = createPoint(4, 2);
        Assertions.assertEquals(DistanceManagementService.getInstance().distance(a, b, c), 4);

    }

    @Test
    public void shouldReturnMinimalDistanceBetweenALineAndAPoint() {
        Point a = createPoint(5, 2);
        Point b = createPoint(6, 2);
        Point c = createPoint(4, 2);
        Point c1 = createPoint(4, 3);
        Point c2 = createPoint(4, 4);
        List<Point> points = Arrays.asList(c, c1, c2);
        Assertions.assertEquals(DistanceManagementService.getInstance().minimalDistance(a, b, points), c);

    }

    @Test
    public void shouldComputeTotalDistance() {
        double totalDistance = DistanceManagementService.getInstance().computeDistance(createPoint(0, 0), createPoint(0, 2), createPoint(0, 5), createPoint(0, 99));
        Assertions.assertEquals(totalDistance, 99d);
    }

    public Point createPoint(int x, int y) {
        Point a = new Point();
        a.setX(x);
        a.setY(y);
        return a;
    }
}
