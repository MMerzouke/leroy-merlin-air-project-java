package models;

import com.opencsv.bean.CsvBindByName;
import models.common.Point;

public class Customer extends Point {
    @CsvBindByName(column = "CustomerId")
    String id;

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Drones[" + id + "]" + " (" + x + "," + y + ")";
    }

}
