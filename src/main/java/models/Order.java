package models;

import com.opencsv.bean.CsvBindAndJoinByName;
import com.opencsv.bean.CsvBindByName;
import org.apache.commons.collections4.MultiValuedMap;

public class Order {
    @CsvBindByName(column = "OrderId")
    public String id;

    @CsvBindByName(column = "CustomerId")
    public String customerId;

    @CsvBindAndJoinByName(column = "ProductId", elementType = String.class)
    public MultiValuedMap<String, String> products;

    @CsvBindAndJoinByName(column = "Quantity", elementType = Integer.class)
    public MultiValuedMap<String, Integer> quantity;

    public String getId() {
        return id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public MultiValuedMap<String, String> getProducts() {
        return products;
    }

    public MultiValuedMap<String, Integer> getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "Order[" + id + "]: from customer[" + customerId + "]";
    }


}
