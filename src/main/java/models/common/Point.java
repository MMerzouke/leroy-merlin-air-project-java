package models.common;

import com.opencsv.bean.CsvBindByName;

/**
 * Represents a point in 2-dimensional Euclidean space
 */
public class Point {

    @CsvBindByName(column = "x")
    protected int x;

    @CsvBindByName(column = "y")
    protected int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
