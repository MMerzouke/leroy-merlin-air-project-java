package models;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class FlightPlan {
    @CsvBindByPosition(position = 0)
    @CsvBindByName(column = "DroneId")
    public String droneId;

    @CsvBindByPosition(position = 1)
    @CsvBindByName(column = "StoreId")
    public String storeId;

    @CsvBindByPosition(position = 2)
    @CsvBindByName(column = "ProductId")
    public String productId;

    @CsvBindByPosition(position = 3)
    @CsvBindByName(column = "CustomerId")
    public String customerId;

    public FlightPlan(String droneId, String storeId, String productId, String customerId) {
        this.storeId = storeId;
        this.droneId = droneId;
        this.productId = productId;
        this.customerId = customerId;
    }
}
