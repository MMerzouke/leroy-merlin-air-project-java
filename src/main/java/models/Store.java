package models;

import com.opencsv.bean.CsvBindByName;
import models.common.Point;

public class Store extends Point {

    @CsvBindByName(column = "StoreId")
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Store[" + id + "]" + " (" + x + "," + y + ")";
    }
}
