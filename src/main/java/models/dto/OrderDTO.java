package models.dto;

import models.Order;
import services.helper.MapHelper;

import java.util.HashMap;

public class OrderDTO {

    private Order order;

    private HashMap<String, Integer> quantityByProduct;

    public OrderDTO(Order order) {
        this.order = order;
        quantityByProduct = MapHelper.mergeListsToMap(order.products.values().stream().toList(), order.quantity.values().stream().toList());
    }

    public Order getOrder() {
        return order;
    }

    public HashMap<String, Integer> getQuantityByProduct() {
        return quantityByProduct;
    }
}
