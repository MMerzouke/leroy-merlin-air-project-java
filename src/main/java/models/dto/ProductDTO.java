package models.dto;

import models.Product;
import services.helper.MapHelper;

import java.util.HashMap;

public class ProductDTO {

    private Product product;

    private HashMap<String, Integer> quantityByStore;

    public ProductDTO(Product product) {
        this.product = product;
        //we build a simplified model to manipulate the stocks
        //REVIEW : should rework ORM
        quantityByStore = MapHelper.mergeListsToMap(product.stores.values().stream().toList(), product.quantity.values().stream().toList());
    }

    public Product getProduct() {
        return product;
    }

    public HashMap<String, Integer> getQuantityByStore() {
        return quantityByStore;
    }
}
