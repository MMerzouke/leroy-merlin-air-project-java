package models.dto;

import models.Drone;

public class DroneDTO extends Drone {
    public double enery;

    public DroneDTO(Drone drone) {
        this.enery = 100;
        this.id = drone.getId();
        this.x = drone.getX();
        this.y = drone.getY();
    }

    public double getEnery() {
        return enery;
    }

    public void setEnery(double enery) {
        this.enery = enery;
    }
}
