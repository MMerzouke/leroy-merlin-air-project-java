package models;

import com.opencsv.bean.CsvBindByName;
import models.common.Point;

public class Drone extends Point {
    @CsvBindByName(column = "DroneId")
    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Drones[" + id + "]" + " (" + x + "," + y + ")";
    }
}
