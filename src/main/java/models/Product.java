package models;

import com.opencsv.bean.CsvBindAndJoinByName;
import com.opencsv.bean.CsvBindByName;
import org.apache.commons.collections4.MultiValuedMap;

public class Product {
    @CsvBindByName(column = "ProductId")
    public String id;

    @CsvBindByName(column = "Name")
    public String name;

    @CsvBindAndJoinByName(column = "StoreId", elementType = String.class)
    public MultiValuedMap<String, String> stores;

    @CsvBindAndJoinByName(column = "Quantity", elementType = Integer.class)
    public MultiValuedMap<String, Integer> quantity;

    @Override
    public String toString() {
        return "Product[" + id + "]";
    }


}
