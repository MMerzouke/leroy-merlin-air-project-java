package services.orchestrator;

import exception.NoDronesAvailableException;
import exception.OutOfStockException;
import models.Customer;
import models.FlightPlan;
import models.Order;
import models.Store;
import models.dto.DroneDTO;
import models.dto.OrderDTO;
import org.apache.commons.lang3.StringUtils;
import repositories.CustomersRepository;
import repositories.FlightPlansRepository;
import repositories.OrdersRepository;
import services.interfaces.IDistanceManagement;
import services.interfaces.IDronesServices;
import services.interfaces.ILogger;
import services.interfaces.IStocksService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * This class handles drones delivery.
 * We retrieve the closest drone from the store and customer with enough energy, and generates the flight plans.
 * Here we try to optimize the time of the flight plan, we could also try to favor stocks, in particular we could imagine that orders have a status to know if the customer is in a hurry or not.
 *
 * We do not manage the status of orders.
 *
 * We don't persist new stocks
 */
public class DeliverOrchestrator {

    ILogger logger;

    IDistanceManagement distanceManagement;
    IDronesServices dronesServices;
    IStocksService stocksService;


    List<Order> orders;
    List<Customer> customers;


    public DeliverOrchestrator(ILogger logger, IDistanceManagement distanceManagement, IDronesServices dronesServices, IStocksService stocksService) {
        this.distanceManagement = distanceManagement;
        this.logger = logger;
        this.dronesServices = dronesServices;
        this.stocksService = stocksService;
        customers = CustomersRepository.getInstance().retrieveCustomers();
        orders = OrdersRepository.getInstance().retrieveOrders();
        this.logger.log("Delivery Set up : Ready for ACTION ! ");
        makeItHappn(); //to improve - the buisness method in a constructor is not a good practice, allow me to avoid the call of a "proxy" method
    }

    private void makeItHappn() {
        List<FlightPlan> flightPlans = new ArrayList<>(); //result holder

        List<OrderDTO> orderDTOS = orders.stream().map(OrderDTO::new).collect(Collectors.toList());
        List<DroneDTO> drones = dronesServices.getAvailableDrones();


        for (OrderDTO order : orderDTOS) {
            Customer customer = customers.stream().filter(customer1 -> customer1.getId().equals(order.getOrder().customerId)).findFirst().get();
            order.getQuantityByProduct().keySet().stream().forEach(productId -> {
                if (StringUtils.isNotEmpty(productId)) { //strange someProductId are empty - need to rework data serialisation???
                    int quantity = order.getQuantityByProduct().get(productId);
                    while (quantity > 0) {
                        try {
                            Store store = stocksService.getStoreByProductId(productId);
                            this.logger.log("Searching a drone... Next Delivery Informations : Order " + order.getOrder().getId() + " | Product " + productId + " | Customer " + customer.getId());
                            DroneDTO drone = getTheClosestDroneWithEnergy(customer, store, drones);
                            this.logger.log("Drone Find ! Next Delivery Informations : Order " + order.getOrder().getId() + " | Product " + productId + " | Customer " + customer.getId() + " | Drone " + drone.getId());
                            flightPlans.add(new FlightPlan(drone.getId(), store.getId(), productId, customer.getId()));
                            dronesServices.updateDronePosition(drone.getId(), store.getX(), store.getY(), distanceManagement.distance(drone, store));
                            dronesServices.updateDronePosition(drone.getId(), customer.getX(), customer.getY(), distanceManagement.distance(drone, customer));//here we update twice the drone position, to compute the right amount of energy
                            quantity--;
                        } catch (OutOfStockException | NoDronesAvailableException e) {
                            this.logger.error("An error occured ", e); //to improve : better deal with the case where there are no more stocks or drones available
                        }
                    }
                }
            });
        }
        FlightPlansRepository.getInstance().writeResult(flightPlans, FlightPlan.class); //to improve : the service orchestrator should refer to a service and not directly to the ORM
    }

    /**
     * Tries to find the closest drones with enough energy to make the deliver
     * @param customer
     * @param store
     * @param drones
     * @return
     * @throws NoDronesAvailableException
     */
    private DroneDTO getTheClosestDroneWithEnergy(Customer customer, Store store, List<DroneDTO> drones) throws NoDronesAvailableException {
        DroneDTO drone = (DroneDTO) distanceManagement.minimalDistance(customer, store, drones);
        double provisionalEnergy = distanceManagement.computeDistance(drone, store, customer);
        if (drones.isEmpty()) {
            throw new NoDronesAvailableException("There is no more drones");
        }
        if (drone.getEnery() - provisionalEnergy < 0) {
            drones.remove(drone);
            return getTheClosestDroneWithEnergy(customer, store, drones);
        } else {
            return drone;
        }
    }
}
