package services.helper;

import java.util.HashMap;
import java.util.List;

public class MapHelper {

    public static HashMap<String, Integer> mergeListsToMap(List<String> keys, List<Integer> values) {
        HashMap<String, Integer> map = new HashMap();
        for (int i = 0; i < keys.size(); i++) {
            map.put(keys.get(i), values.get(i));
        }
        return map;
    }
}
