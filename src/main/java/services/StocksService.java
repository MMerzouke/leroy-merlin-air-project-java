package services;

import exception.OutOfStockException;
import models.Store;
import models.dto.ProductDTO;
import repositories.ProductsRepository;
import repositories.StoresRepository;
import services.interfaces.ILogger;
import services.interfaces.IStocksService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class StocksService implements IStocksService {
    public static StocksService instance;
    public List<ProductDTO> products;
    public List<Store> stores;

    private ILogger logger;

    private StocksService(ILogger logger, StoresRepository storesRepository, ProductsRepository productsRepository) {
        this.logger = logger;
        stores = storesRepository.retrieveStores();
        products = productsRepository.retrieveProducts().stream().map(ProductDTO::new).collect(Collectors.toList());
    }

    ;

    public static StocksService getInstance(StoresRepository storesRepository, ProductsRepository productsRepository) {
        if (instance == null)
            instance = new StocksService(new Logger("StockServices"), storesRepository, productsRepository);
        return instance;
    }

    /**
     * Return the store where we can find the most quantity of productId
     *
     * @param productId
     * @return
     * @throws OutOfStockException
     */
    @Override
    public Store getStoreByProductId(String productId) throws OutOfStockException {
        ProductDTO product = products.stream().filter(productDTO -> productDTO.getProduct().id.equals(productId)).findFirst().get();
        String storeId = Collections.max(product.getQuantityByStore().entrySet(), (entry1, entry2) -> entry1.getValue() - entry2.getValue()).getKey();
        if (product.getQuantityByStore().get(storeId).equals(0)) {
            throw new OutOfStockException("Product(" + productId + ") has no more stock in any store");
        }
        return stores.stream().filter(store -> store.getId().equals(storeId)).findFirst().get();
    }

    @Override
    public boolean updateStock(String storeId, String productId) {
        ProductDTO product = products.stream().filter(productDTO -> productDTO.getProduct().id.equals(productId)).findFirst().get();
        product.getQuantityByStore().put(storeId, product.getQuantityByStore().get(storeId) - 1); //drones can only move one item at a time hence the -1
        return true;
    }


}
