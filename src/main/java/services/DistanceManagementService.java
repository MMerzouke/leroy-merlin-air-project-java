package services;

import models.common.Point;
import services.interfaces.IDistanceManagement;

import java.util.Comparator;
import java.util.List;

public class DistanceManagementService implements IDistanceManagement {

    public static DistanceManagementService instance;

    private DistanceManagementService() {
    }

    ;

    public static DistanceManagementService getInstance() {
        if (instance == null) instance = new DistanceManagementService();
        return instance;
    }

    @Override
    public double distance(Point A, Point B) {
        return Math.abs(
                Math.sqrt(
                        Math.pow(B.getY() - A.getY(), 2)
                                +
                                Math.pow(B.getX() - A.getX(), 2)
                )
        );
    }

    @Override
    public double distance(Point A, Point B, Point C) {
        double m = (B.getY() - A.getY()) / (B.getX() - A.getX());
        double p = B.getY() - m * B.getX();
        return Math.abs(
                (C.getY() - m * C.getX() + p)
                        /
                        Math.sqrt(Math.pow(m, 2) + 1)
        );
    }

    @Override
    public Point minimalDistance(Point A, Point B, List<? extends Point> points) {
        return points.stream().min(Comparator.comparing(C -> distance(A, B, C))).get();
    }

    @Override
    public double computeDistance(Point... points) {
        double totalDistance = 0;
        for (int i = 0; i < points.length - 1; i++) {
            totalDistance += distance(points[i], points[i + 1]);
        }
        return totalDistance;
    }


}
