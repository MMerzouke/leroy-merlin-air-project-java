package services;

import models.dto.DroneDTO;
import repositories.DronesRepository;
import services.interfaces.IDronesServices;
import services.interfaces.ILogger;

import java.util.List;
import java.util.stream.Collectors;

public class DronesServices implements IDronesServices {
    public static DronesServices instance;
    private static ILogger logger;
    private List<DroneDTO> droneDTOs;

    private DronesServices(ILogger logger, DronesRepository dronesRepository) {
        this.logger = logger;
        droneDTOs = dronesRepository.retrieveDrones().stream().map(DroneDTO::new).collect(Collectors.toList());
    }

    public static DronesServices getInstance(DronesRepository dronesRepository) {
        if (instance == null) instance = new DronesServices(new Logger("DronesServices"), dronesRepository);
        return instance;
    }

    public static DronesServices getInstance(ILogger logger, DronesRepository dronesRepository) {
        if (instance == null) instance = new DronesServices(logger, dronesRepository);
        return instance;
    }

    @Override
    public List<DroneDTO> getAvailableDrones() {
        logger.info("Send all available drones informations");
        return droneDTOs;
    }

    @Override
    public boolean updateDronePosition(String droneId, int x, int y, double energyConsumed) {
        DroneDTO drone = droneDTOs.stream().filter(droneDTO -> droneDTO.getId().equals(droneId)).findFirst().get();
        double newEnergy = drone.getEnery() - energyConsumed;
        if (newEnergy < 0) {
            logger.info("Drone[" + droneId + "] can't move anymore");
            updateAvailableDroneList(drone);
            return false;
        }
        if (newEnergy == 0) {
            logger.info("Drone[" + droneId + "] has just enoug energy");
            updateAvailableDroneList(drone);
        }
        drone.setX(x);
        drone.setY(y);
        drone.setEnery(newEnergy);
        logger.info("Drone[" + droneId + "], energy left " + newEnergy);
        return true;
    }

    private void updateAvailableDroneList(DroneDTO droneDTO) {
        this.droneDTOs.remove(droneDTO);
        logger.info("Drone[" + droneDTO.getId() + "] removed from available drones list");
    }
}
