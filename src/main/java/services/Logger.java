package services;

import services.interfaces.ILogger;

public class Logger implements ILogger {

    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";

    private final String TAG;

    public Logger(String tag) {
        this.TAG = tag;
    }

    public void log(String message) {
        System.out.println(TAG + ": " + message);
    }

    public void info(String message) {
        System.out.println("🦄  " + ANSI_BLUE + TAG + ": " + message + ANSI_RESET);
    }

    public void error(String message, Throwable t) {
        System.err.println(t.getStackTrace());
        System.err.println(t.getMessage());
        System.err.println("💩  " + TAG + " : " + message);
    }


}
