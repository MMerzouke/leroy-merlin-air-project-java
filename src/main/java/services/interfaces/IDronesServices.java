package services.interfaces;

import models.dto.DroneDTO;

import java.util.List;

public interface IDronesServices {

    public List<DroneDTO> getAvailableDrones();

    boolean updateDronePosition(String droneId, int x, int y, double energyConsumed);
}
