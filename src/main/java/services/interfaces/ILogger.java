package services.interfaces;

public interface ILogger {
    void log(String message);

    void error(String message, Throwable t);

    void info(String message);

}
