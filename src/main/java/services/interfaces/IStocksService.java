package services.interfaces;

import exception.OutOfStockException;
import models.Store;

public interface IStocksService {
    public Store getStoreByProductId(String productId) throws OutOfStockException;

    public boolean updateStock(String storeId, String productId);
}
