package services.interfaces;

import models.common.Point;

import java.util.List;

public interface IDistanceManagement {
    /**
     * Returns the distance between 2 points
     */
    double distance(Point A, Point B);

    /**
     * Returns the distance between a line (AB) and a point C
     */
    double distance(Point A, Point B, Point C);

    /**
     * Returns a point in list C closest to the other 2 points A and B
     *
     * @param A      Point A (for instance a store)
     * @param B      Point B (for instance a customer)
     * @param points List Points C (for instance the drones)
     * @return
     */
    Point minimalDistance(Point A, Point B, List<? extends Point> points);

    double computeDistance(Point... points);
}
