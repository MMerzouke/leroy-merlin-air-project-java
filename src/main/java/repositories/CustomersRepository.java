package repositories;

import models.Customer;
import repositories.common.CsvRepository;

import java.nio.file.Paths;
import java.util.List;

public class CustomersRepository extends CsvRepository<Customer> {

    private static CustomersRepository customerRepository;

    private CustomersRepository() {
        super(Paths.get("").toAbsolutePath() + "\\src\\main\\resources\\csv\\customers.csv", Customer.class);
    }

    public static CustomersRepository getInstance() {
        if (customerRepository == null) {
            customerRepository = new CustomersRepository();
        }
        return customerRepository;
    }

    public List<Customer> retrieveCustomers() {
        this.logger.log("Loading Customers...");
        List<Customer> customers = super.csvToBeanBuilder.build().parse();
        this.logger.info(customers.size() + " customers has been successfully loaded");
        this.closeReader();
        return customers;
    }
}
