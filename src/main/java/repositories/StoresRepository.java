package repositories;

import models.Store;
import repositories.common.CsvRepository;

import java.nio.file.Paths;
import java.util.List;

public class StoresRepository extends CsvRepository<Store> {

    private static StoresRepository storeRepository;

    private StoresRepository() {
        super(Paths.get("").toAbsolutePath() + "\\src\\main\\resources\\csv\\stores.csv", Store.class);
    }

    public static StoresRepository getInstance() {
        if (storeRepository == null) {
            storeRepository = new StoresRepository();
        }
        return storeRepository;
    }

    public List<Store> retrieveStores() {
        this.logger.log("Loading Stores...");
        List<Store> stores = super.csvToBeanBuilder.build().parse();
        this.logger.info(stores.size() + " stores has been successfully loaded");
        this.closeReader();
        return stores;
    }
}
