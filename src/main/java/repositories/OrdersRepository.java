package repositories;

import models.Order;
import repositories.common.CsvRepository;

import java.nio.file.Paths;
import java.util.List;

public class OrdersRepository extends CsvRepository<Order> {
    private static OrdersRepository ordersRepository;

    private OrdersRepository() {
        super(Paths.get("").toAbsolutePath() + "\\src\\main\\resources\\csv\\orders.csv", Order.class);
    }

    public static OrdersRepository getInstance() {
        if (ordersRepository == null) {
            ordersRepository = new OrdersRepository();
        }
        return ordersRepository;
    }

    public List<Order> retrieveOrders() {
        this.logger.log("Loading Orders...");
        List<Order> orders = csvToBeanBuilder.build().parse();
        this.logger.info(orders.size() + " products has been successfully loaded");
        this.closeReader();
        return orders;
    }
}
