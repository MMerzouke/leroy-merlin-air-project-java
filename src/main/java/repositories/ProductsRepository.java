package repositories;

import models.Product;
import repositories.common.CsvRepository;

import java.nio.file.Paths;
import java.util.List;

public class ProductsRepository extends CsvRepository<Product> {
    private static ProductsRepository productsRepository;

    private ProductsRepository() {
        super(Paths.get("").toAbsolutePath() + "\\src\\main\\resources\\csv\\products.csv", Product.class);
    }

    public static ProductsRepository getInstance() {
        if (productsRepository == null) {
            productsRepository = new ProductsRepository();
        }
        return productsRepository;
    }

    public List<Product> retrieveProducts() {
        this.logger.log("Loading Products...");
        List<Product> products = super.csvToBeanBuilder.build().parse();
        this.logger.info(products.size() + " stores has been successfully loaded");
        this.closeReader();
        return products;
    }
}
