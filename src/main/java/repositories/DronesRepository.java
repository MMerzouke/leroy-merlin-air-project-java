package repositories;

import models.Drone;
import repositories.common.CsvRepository;

import java.nio.file.Paths;
import java.util.List;

public class DronesRepository extends CsvRepository<Drone> {

    private static DronesRepository dronesRepository;

    private DronesRepository() {
        super(Paths.get("").toAbsolutePath() + "\\src\\main\\resources\\csv\\drones.csv", Drone.class);
    }

    public static DronesRepository getInstance() {
        if (dronesRepository == null) {
            dronesRepository = new DronesRepository();
        }
        return dronesRepository;
    }

    public List<Drone> retrieveDrones() {
        this.logger.log("Loading Drones...");
        List<Drone> drones = super.csvToBeanBuilder.build().parse();
        this.logger.info(drones.size() + " drones has been successfully loaded");
        this.closeReader();
        return drones;
    }
}
