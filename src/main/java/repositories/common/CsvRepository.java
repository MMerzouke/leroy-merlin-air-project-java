package repositories.common;

import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import services.Logger;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class will be used for all CSV actions: read and write
 * In a process of simplification we will not try to update and delete
 *
 * @param <T> The type we want to serialize
 */
public class CsvRepository<T> {

    protected CsvToBeanBuilder<T> csvToBeanBuilder;
    protected Logger logger;
    private FileReader fr;
    private String filePath;

    public <T> CsvRepository(String filePath, Class className, Logger logger) {
        this.logger = logger;
        this.filePath = filePath;
        try {
            this.fr = new FileReader(filePath);
            csvToBeanBuilder = new CsvToBeanBuilder<T>(fr).withType(className).withSeparator(';');
        } catch (IOException e) {
            this.logger.error("IOEception :" + filePath, e);
        }
    }

    protected <T> CsvRepository(String filePath, Class className) {
        this(filePath, className, new Logger(className.getSimpleName()));
    }

    protected void closeReader() {
        try {
            this.fr.close();
        } catch (IOException | NullPointerException e) {
            this.logger.error("IOEception : while closing reading input stream", e);
        }
    }

    public <T> boolean writeResult(List<T> beans, Class clazz) {
        try {
            Writer writer = new FileWriter(filePath);
            StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer).withSeparator(';').build();
            writer.append(buildHeader(clazz));
            sbc.write(beans);
            writer.close();
        } catch (IOException e) {
            this.logger.error("IOEception :" + filePath, e);
            return false;
        } catch (CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
            this.logger.error("CsvExeption :" + filePath, e);
        }
        return true;
    }

    private String buildHeader(Class<T> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.getAnnotation(CsvBindByPosition.class) != null
                        && f.getAnnotation(CsvBindByName.class) != null)
                .sorted(Comparator.comparing(f -> f.getAnnotation(CsvBindByPosition.class).position()))
                .map(f -> f.getAnnotation(CsvBindByName.class).column())
                .collect(Collectors.joining(";")) + "\n";
    }


}
