package repositories;

import models.FlightPlan;
import repositories.common.CsvRepository;

import java.nio.file.Paths;

public class FlightPlansRepository extends CsvRepository<FlightPlan> {
    private static FlightPlansRepository flightPlansRepository;

    protected <T> FlightPlansRepository() {
        super(Paths.get("").toAbsolutePath() + "\\src\\main\\resources\\csv\\flight-plans.csv", FlightPlan.class);
    }

    public static FlightPlansRepository getInstance() {
        if (flightPlansRepository == null) {
            flightPlansRepository = new FlightPlansRepository();
        }

        return flightPlansRepository;
    }


}
