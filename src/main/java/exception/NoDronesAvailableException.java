package exception;

public class NoDronesAvailableException extends Exception {
    public NoDronesAvailableException(String message) {
        super(message);
    }

}
