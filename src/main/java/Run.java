import repositories.DronesRepository;
import repositories.ProductsRepository;
import repositories.StoresRepository;
import services.DistanceManagementService;
import services.DronesServices;
import services.Logger;
import services.StocksService;
import services.orchestrator.DeliverOrchestrator;

import java.io.FileNotFoundException;


public class Run {

    public static void main(String[] args) throws FileNotFoundException {
        Logger logger = new Logger("DeliverOrchestrator");
        DeliverOrchestrator deliverOrchestrator = new DeliverOrchestrator(
                logger,
                DistanceManagementService.getInstance(),
                DronesServices.getInstance(DronesRepository.getInstance()),
                StocksService.getInstance(StoresRepository.getInstance(), ProductsRepository.getInstance()));
    }
}
